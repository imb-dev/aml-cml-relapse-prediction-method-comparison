function [] = noise_advanced_scheme_nd(total)

%total = false;

if total
    load('../data/NoiseData_100k.mat','NoiseData')
else
    load('../data/NoiseData.mat','NoiseData')
end
NoiseAdvNDData = [];

s = RandStream('mt19937ar','Seed',1);
%% advanced scheme: measuring at cycle start + every 6 weeks afterwards

ids = unique(NoiseData.id,'stable');

for i=1:length(ids)
    idxs = find(NoiseData.id==ids(i));
    
    t_measurement = str2num(NoiseData.chemo_begin(idxs(1)));
    t_measurement = [t_measurement t_measurement(end)+7*6:6*7:NoiseData.maxTime(idxs(1))];
    t_measurement = unique(t_measurement);
    
    for j=1:length(t_measurement)
        i_min_diff(j) = find(NoiseData.time(idxs)<=t_measurement(j),1,'last');
    end
    i_min_diff = unique(i_min_diff);
    NoiseAdvNDData = [NoiseAdvNDData; NoiseData(idxs(i_min_diff),:)];
    clear i_min_diff
end

NoiseAdvNDData.log10NPM1_ABL(NoiseAdvNDData.log10NPM1_ABL<-5) = -5;

if total
    save('../data/NoiseAdvNDData_100k.mat','NoiseAdvNDData')
%     writetable(NoiseAdvNDData,'../../pythonfiles/data/NoiseAdvNDData_100k.csv')
else
    save('../data/NoiseAdvNDData.mat','NoiseAdvNDData')
%     writetable(NoiseAdvNDData,'../../pythonfiles/data/NoiseAdvNDData.csv')
end