clear all
close all

load('data/data.mat')

VarNames = {'time','log10NPM1_ABL','chemo_num','chemo_begin','chemo_end' 'rlps_time' 'relapse' 'id'};
datacsv = table('Size',[0 8],'VariableTypes',{'int16' 'double' 'int8' 'string' 'string' 'double' 'string' 'string'},...
    'VariableNames',VarNames);

n=0;
rlps=0;

for i=1:size(data,2)
    
    time = data(i).NPM1.time;
    value = log10(data(i).NPM1.value);
    value(time>9*30) = [];
    time(time>9*30) = [];
    
    if length(time)<3; continue; end
    
    chemo_num = data(i).Chemo.num;
    chemo_begin = string(num2str(data(i).Chemo.begin));
    chemo_end = string(num2str(data(i).Chemo.end));
    rlps_time = data(i).Relapse.time_corr;
    
    if rlps_time==0
        continue;
    end
    
    if data(i).Relapse.flag==1 && rlps_time<=24*30
        rlps_flag = "True";
        rlps = rlps+1;
    elseif data(i).charact.rem_time>=24*30
        rlps_flag = "False";
    else
        continue;
    end
    id = data(i).PatientID;
    
    datacsv = [datacsv; table(time,value,repmat(chemo_num,size(time)),repmat(chemo_begin,size(time)),repmat(chemo_end,size(time)),...
        repmat(rlps_time,size(time)),repmat(rlps_flag,size(time)),repmat(id,size(time)),'VariableNames',VarNames)];
    
    n=n+1;
end

writetable(datacsv,'data/RealData.csv')
RealData = datacsv;
save('data/RealData.mat','RealData')

copyfile('data/RealData.csv','/Users/hoffmann/Documents/Paper/Predictions/pythonfiles/data')