function [] = noise_sparse(total)

%total=false;

if total
    load('../data/NoiseData_100k.mat','NoiseData')
else
    load('../data/NoiseData.mat','NoiseData')
end

NoiseSparseData = [];

s = RandStream('mt19937ar','Seed',1);
%% include sparseness of Data similar to the sparseness of the real data

load('../data/data.mat','data')
times_total = extractfield(cell2mat(extractfield(data,'NPM1')),'time');
times_total = times_total(times_total<=720);
times1 = times_total(times_total<=3*30);
times2 = times_total(times_total<=6*30 & times_total>3*30);
times3 = times_total(times_total<=9*30 & times_total>6*30);

[h1,~] = histcounts(times1,ceil(3*30/7));
weight1 = h1/sum(h1);
[h2,~] = histcounts(times2,ceil(3*30/7));
weight2 = h2/sum(h2);
[h3,~] = histcounts(times3,round(3*30/7));
weight3 = h3/sum(h3);

%devide in different periods: first 3 months; second 3 months; thirs 3 months.

%first 3 months
num1 = cellfun(@(x) length(find(x.time<=3*30)),{data.NPM1});
%distance1 = vertcat([cellfun(@(x) diff(x.time(x.time<=3*30)),{data.NPM1},'UniformOutput',false)]);

%second 3 months
num2 = cellfun(@(x) length(find(x.time<=6*30 & x.time>3*30)),{data.NPM1});

%third 3 months
num3 = cellfun(@(x) length(find(x.time<=9*30 & x.time>6*30)),{data.NPM1});

ids = unique(NoiseData.id,'stable');

num1_sample = datasample(s,num1,length(ids));
num2_sample = datasample(s,num2,length(ids));
num3_sample = datasample(s,num3,length(ids));


for i=1:length(ids)
    
    idxs = find(NoiseData.id==ids(i));
    %first 6 months
    idx1 = sort(datasample(s,idxs(NoiseData(idxs,:).time<=3*30),num1_sample(i),'Replace',false,'Weights',weight1));
    aData = NoiseData(idx1,:);
    %second 6 months
    idx2 = sort(datasample(s,idxs(NoiseData(idxs,:).time<=6*30 & NoiseData(idxs,:).time>3*30),num2_sample(i),'Replace',false,'Weights',weight2));
    aData = [aData; NoiseData(idx2,:)];
    %second year
    idx3 = sort(datasample(s,idxs(NoiseData(idxs,:).time>6*30),num3_sample(i),'Replace',false,'Weights',weight3));
    aData = [aData; NoiseData(idx3,:)];
    aData.maxTime = repmat(max(aData.time),size(aData.time));
    NoiseSparseData = [NoiseSparseData;aData];
    
end

if total
    save('../data/NoiseSparseData_100k.mat','NoiseSparseData')
%     writetable(NoiseSparseData,'../../pythonfiles/data/NoiseSparseData_100k.csv')
else
    save('../data/NoiseSparseData.mat','NoiseSparseData')
%     writetable(NoiseSparseData,'../../pythonfiles/data/NoiseSparseData.csv')
end