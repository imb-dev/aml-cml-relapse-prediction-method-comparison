load('GLMparFit.mat')

par_art = par_NoiseSparseND;
xlims = [-10 200; -9e2 4e2; -2 5; -0.2 0.1; -7 0];

pars = {'a','b','y0','alpha','n'};

figure()
for i=1:5
    subplot(5,2,1+2*(i-1))
    %h(i) = histogram(par_art(:,i));
    %histfit(par_art(:,i),10,'kernel')
    boxplot(par_art(:,i),'symbol','')
    ylim(xlims(i,:))
    CreatePlotCSV(zeros(length(par_art(:,i)),1),par_art(:,i),['plots/csvs/par_art_' pars{i}],0);
end
for i=1:5
    subplot(5,2,2*i)
    %histogram(par_Real(:,i),h(i).BinEdges)
    %histfit(par_Real(:,i),10,'kernel')
    boxplot(par_Real(:,i),'symbol','')
    ylim(xlims(i,:))
    CreatePlotCSV(zeros(length(par_Real(:,i)),1),par_Real(:,i),['plots/csvs/par_Real_' pars{i}],0);
end

%CreatePlotCSV(par_art(:,1),zeros(length(par_art(:,1)),1),['../plots/csvs/par_art'],0);