function [] = noise_sparse_nd(total)

%total = false;

if total
    load('../data/NoiseSparseData_100k.mat','NoiseSparseData')
else
    load('../data/NoiseSparseData.mat','NoiseSparseData')
end

NoiseSparseNDData = NoiseSparseData; 
%% set detection limit to 1e-3

NoiseSparseNDData.log10NPM1_ABL(NoiseSparseNDData.log10NPM1_ABL<-5) = -5;

if total
    save('../data/NoiseSparseNDData_100k.mat','NoiseSparseNDData')
%     writetable(NoiseSparseNDData,'../../pythonfiles/data/NoiseSparseNDData_100k.csv')
else
    save('../data/NoiseSparseNDData.mat','NoiseSparseNDData')
%     writetable(NoiseSparseNDData,'../../pythonfiles/data/NoiseSparseNDData.csv')
end