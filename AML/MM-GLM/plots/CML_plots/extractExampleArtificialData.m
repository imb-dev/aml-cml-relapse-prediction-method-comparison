clear all

patient = '8r';

D = readtable('../../data/NNData/fullData.csv');
DN = readtable('../../data/NNData/noiseData.csv');
DNS = readtable('../../data/NNData/noiseSparseData.csv');
AP = readtable('../../data/NNData/noiseSparseUdData.csv');
AS = readtable('../../data/NNData/noiseSparseUdDataHD.csv');

i_D = find(string(D.id)==patient);
writetable(D(i_D,:),'../../data/CML_artificial_data/D.csv');

i_DN = find(string(DN.id)==patient);
writetable(DN(i_DN,:),'../../data/CML_artificial_data/DN.csv');

i_DNS = find(string(DNS.id)==patient);
writetable(DNS(i_DNS,:),'../../data/CML_artificial_data/DNS.csv');

i_AP = find(string(AP.id)==patient);
writetable(AP(i_AP,:),'../../data/CML_artificial_data/AP.csv');

i_AS = find(string(AS.id)==patient);
writetable(AS(i_AS,:),'../../data/CML_artificial_data/AS.csv');