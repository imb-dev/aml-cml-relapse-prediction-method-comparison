% kreiere so viele neue samples indem ein standartfehler von 0.5 auf die
% Werte gerechnet wird, dass am Ende 500 relapse und 500 non-relapse
% samples da sind

clear all
close all

n_samples = 500;

load('data/RealData.mat')

rlps = 0;
non_rlps = 0;

VarNames = {'time','log10NPM1_ABL','chemo_num','chemo_begin','chemo_end' 'rlps_time' 'relapse' 'id'};
datacsv = table('Size',[0 8],'VariableTypes',{'int16' 'double' 'int8' 'string' 'string' 'double' 'string' 'string'},...
    'VariableNames',VarNames);

i=1;

ids = unique(RealData.id,'stable');

%for reproducability
s = RandStream('mt19937ar','Seed',1);
RandStream.setGlobalStream(s);
rng(1);

while rlps<n_samples || non_rlps<n_samples%for i=1:size(data,2)
    
    ind = find(RealData.id==ids(i));
    
    time = RealData.time(ids);
    value = RealData.log10NPM1_ABL(ids);
    
    value = value+normrnd(0,0.5,size(value));
    
    chemo_num = RealData.chemo_num(ind(1));
    chemo_begin = RealData.chemo_begin(ind(1));
    chemo_end = RealData.chemo_end(ind(1));
    rlps_time = NaN;
    if data(i).Relapse.flag==1 && rlps< n_samples
        rlps_flag = "True";
        id = [num2str(data(i).PatientID),num2str(rlps+1)];
    elseif non_rlps < n_samples
        rlps_flag = "False";
        id = [num2str(data(i).PatientID),num2str(non_rlps+1)];
    else
        if i==length(ids); i=1;
        else; i=i+1;
        end
        continue;
    end
    
    
    datacsv = [datacsv; table(time,value,repmat(chemo_num,size(time)),repmat(chemo_begin,size(time)),repmat(chemo_end,size(time)),...
        repmat(rlps_time,size(time)),repmat(rlps_flag,size(time)),repmat(id,size(time)),'VariableNames',VarNames)];
    
    if strcmp(rlps_flag,"True"); rlps = rlps+1;
    else; non_rlps = non_rlps+1;
    end
    
    if i==size(data,2); i=1;
    else; i=i+1;
    end
    
end



writetable(datacsv,'data/RealData_aug.csv')
RealData_aug = datacsv;
save('data/RealData_aug.mat','RealData_aug')

copyfile('data/RealData_aug.csv','/Users/hoffmann/Documents/Paper/Predictions/pythonfiles/data')