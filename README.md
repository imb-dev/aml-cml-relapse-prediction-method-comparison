# Summary

Risk stratification and treatment decisions for leukaemia patients are regularly based on clinical markers determined at diagnosis, while measurements on system dynamics are often neglected. However, there is increasing evidence that linking quantitative time-course information to disease outcomes can improving the predictions for patient-specific treatment response.
We analyzed the potential of different computational methods to accurately predict relapse for chronic and acute myeloid leukaemia, particularly focusing on the influence of data quality and
quantity. Technically, we used clinical reference data to generate in-silico patients with varying
levels of data quality. Based hereon, we compared the performance of mechanistic models, generalized linear models, and neural networks with respect to their accuracy for relapse prediction. We found that data quality has a higher impact on prediction accuracy than the
specific choice of the method. We further show that adapted treatment and measurement schemes can considerably improve prediction accuracy. Our proof-of-principle study highlights
how computational methods and optimized data acquisition strategies can improve risk
assessment and treatment of leukaemia patients.

# Instructions

## AML

NOTE: All files are in a work-in-progress style and might not be executable without adaptation.

### Data generation

For artificial data generation execute: `AML/MM-GLM/data_generation/data_generation.m` (MATLAB)

### Mechanistic model

To fit the mechanistic model to artificial data set _set_name_: (MATLAB)

- load _set_name_ into workspace
- execute `AML/MM_GLM/Modelfit.m` with input parameter _set_name_
- results are saved as: `AML/MM-GLM/data/result_fit_set_name.csv`

### GLM

To fit GLM to the artificial to all artificial data sets: (MATLAB)

- execute `AML/MM-GLM/GLM.m`
- results are saved as `AML/MM-GLM/GLM_acc.mat`

### Neural networks

To train and test the Neural network with 10-fold crossvalidation: (python)

- execute `AML/NN/AML_final_server_crossval.py`
- results can be evaluated using `AML/MM-GLM/plots/Accuracies/Accuracy.m` in the section `accurcies LSTM` within the file

## CML

NOTE: The patient data was removed and will be supplied upon request from corresponding author
[Ingmar Glauche](mailto:ingmar.glauche@tu-dresden.de).

### Data generation

For artificial data generation execute the following:

1. `cd CML/MM-GLM/generateData/DS`
2. `Rscript PrepareData.R`
3. `Rscript fitPatients.R`
4. `cd ../../`
5. `Rscript generateData.R`

### Mechanistic model

To fit the mechanistic model to artificial data sets execute the following:

1. `cd CML/MM-GLM/`
2. `Rscript fitModel.R`

### GLM

To fit GLM to the artificial to artificial data sets execute the following:

1. `cd CML/MM-GLM/`
2. `Rscript fitBiexp.R`
3. `Rscript glm.R`

### Neural networks

To train and test the Neural network with 10-fold crossvalidation: (python)

- execute `CML/NN/CML_LSTM_server_crossval.py`
- results can be evaluated using `AML/MM-GLM/plots/CML_plots/csvsForCML.m` in the section `accurcies LSTM` within the file
