%wandle die Daten von Christoph zur CML in csvs um die dann f�r das Plotten
%in tikz verwendet werden k�nnen

clear all
close all

%% artificial patient data
data_D = readtable('../../data/CML_artificial_data/D.csv');
data_D = table2array(data_D(:,2:3));
CreatePlotCSV(data_D(:,1),10.^data_D(:,2),'../csvs/CML_data_D',0);

data_DN = readtable('../../data/CML_artificial_data/DN.csv');
data_DN= table2array(data_DN(:,2:3));
CreatePlotCSV(data_DN(:,1),10.^data_DN(:,2),'../csvs/CML_data_DN',0);

data_DNS = readtable('../../data/CML_artificial_data/DNS.csv');
data_DNS= table2array(data_DNS(:,2:3));
CreatePlotCSV(data_DNS(:,1),10.^data_DNS(:,2),'../csvs/CML_data_DNS',0);

data_AP_t = readtable('../../data/CML_artificial_data/AP.csv');
data_AP= table2array(data_AP_t(string(table2array(data_AP_t(:,4)))=='FALSE',2:3));
CreatePlotCSV(data_AP(:,1),10.^data_AP(:,2),'../csvs/CML_data_AP',0);
data_AP_nd = table2array(data_AP_t(string(table2array(data_AP_t(:,4)))=='TRUE',2:3));
CreatePlotCSV(data_AP_nd(:,1),10.^data_AP_nd(:,2),'../csvs/CML_data_AP_nd',0);

data_AS = readtable('../../data/CML_artificial_data/AS.csv');
data_AS_p= table2array(data_AS(string(table2array(data_AS(:,4)))=='FALSE',2:3));
CreatePlotCSV(data_AS_p(:,1),10.^data_AS_p(:,2),'../csvs/CML_data_AS',0);
data_AS_nd = table2array(data_AS(string(table2array(data_AS(:,4)))=='TRUE',2:3));
CreatePlotCSV(data_AS_nd(:,1),10.^data_AS_nd(:,2),'../csvs/CML_data_AS_nd',0);


%% bi-exponential fit

data_patient = readtable('../../data/CML_GLM/patient.csv');

%patient data
patient = table2array(data_patient(string(table2array(data_patient(:,4)))=='FALSE',2:3));
CreatePlotCSV(patient(:,1),10.^patient(:,2),'../csvs/CML_bifit_patient',0);
patient_nd = table2array(data_patient(string(table2array(data_patient(:,4)))=='TRUE',2:3));
CreatePlotCSV(patient_nd(:,1),10.^patient_nd(:,2),'../csvs/CML_bifit_patient_nd',0);

%bi-exponential fit
bi_fit = table2array(readtable('../../data/CML_GLM/biexp.csv'));
CreatePlotCSV(bi_fit(:,1),10.^bi_fit(:,2),'../csvs/CML_bifit_fit',0);

%fit during half-dose time
hd_fit = table2array(readtable('../../data/CML_GLM/lm.csv'));
CreatePlotCSV(hd_fit(:,1),10.^hd_fit(:,2),'../csvs/CML_bifit_hdfit',0);

%first exponential
param_fit = readtable('../../data/CML_GLM/params.csv');
x = -4:20;
y = log10(exp(param_fit.A-exp(param_fit.alpha)*x));
CreatePlotCSV(x,10.^y,'../csvs/CML_exfit1',0);

par(1) = exp(param_fit.A);
x_par(1) = 0;

%second exponential
x = -4:129;
y = log10(exp(param_fit.B-param_fit.beta*x));
CreatePlotCSV(x,10.^y,'../csvs/CML_exfit2',0);

par(2) = exp(param_fit.B);
x_par(2) = 0;

%HD fit
x = 115:145;
y = param_fit.g*x+param_fit.C;
CreatePlotCSV(x,10.^y,'../csvs/CML_HDfit',0);

par(3) = 10^y(15);
x_par(3) = x(15);

CreatePlotCSV(x_par,par,'../csvs/CML_bifit_param',0);

%% model fits

bsp_fit = table2array(readtable('../../data/CML_ResultsPatientFits/model.csv'));
CreatePlotCSV(bsp_fit(:,1),10.^bsp_fit(:,2),'../csvs/CML_bsp_fit',0);

bsp_patient = readtable('../../data/CML_ResultsPatientFits/patient.csv');
bsp_patient = table2array(bsp_patient(string(table2array(bsp_patient(:,6)))=='FALSE',3:4));
CreatePlotCSV(bsp_patient(:,1),10.^bsp_patient(:,2),'../csvs/CML_bsp_patient',0);

bsp_patient = readtable('../../data/CML_ResultsPatientFits/patient.csv');
bsp_patient_lQL = 10.^str2double(bsp_patient.lQL(string(table2array(bsp_patient(:,6)))=='TRUE'));
bsp_patient_time = table2array(bsp_patient(string(table2array(bsp_patient(:,6)))=='TRUE',3));
CreatePlotCSV(bsp_patient_time,bsp_patient_lQL,'../csvs/CML_bsp_patient_nd',0);


mae = readtable('../../data/CML_ResultsPatientFits/mae_rss.csv');
mae = sort(table2array(mae(:,2)),'descend');
CreatePlotCSV(1:length(mae),mae,'../csvs/CML_MAE',0);


%% results GLM

results_GLM = readtable('../../data/CML_GLM_results/withoutHD.csv');
results_GLM_AS = readtable('../../data/CML_GLM_results/withHD.csv');
results_MM = readtable('../../data/CML_modelFits/withoutHD.csv');

mean_G = [str2double(table2array(results_GLM(1,2:3:end))),str2double(table2array(results_GLM_AS(1,11)))];
std_G = [str2double(table2array(results_GLM(1,3:3:end))),str2double(table2array(results_GLM_AS(1,12)))];

CreatePlotCSV(0:length(mean_G)-1,mean_G*100,'../csvs/CML_GLM',0,std_G*100)

acc_glm = [str2num(cell2mat(table2array(results_GLM(1,4))))',str2num(cell2mat(table2array(results_GLM(1,7))))',str2num(cell2mat(table2array(results_GLM(1,10))))',str2num(cell2mat(table2array(results_GLM(1,13))))'];
x = [zeros(size(acc_glm(:,1)));ones(size(acc_glm(:,1)));2*ones(size(acc_glm(:,1)));3*ones(size(acc_glm(:,1)))];
x = x+normrnd(0,0.02,size(x));
CreatePlotCSV(x,acc_glm(1:40)'*100,'../csvs/CML_GLM_all',0)

acc_glm = [acc_glm, str2num(cell2mat(table2array(results_GLM_AS(1,13))))'];
%% comparison of parameters for GLM between artificial and real data

results_fitComp_art = readtable('../../data/CML_biexpComparison/statParams-AS.csv');
results_fitComp_real = readtable('../../data/CML_biexpComparison/statParams-DS.csv');

parNames = results_fitComp_art.Properties.VariableNames(2:end);
parNames{2} = 'la_';
for i=1:4
    CreatePlotCSV(zeros(size(results_fitComp_art,1),1),table2array(results_fitComp_art(:,i+1)),['../csvs/CML_parComp_art_' parNames{i}],0);
end
for i=1:4
    CreatePlotCSV(zeros(size(results_fitComp_real,1),1),table2array(results_fitComp_real(:,i+1)),['../csvs/CML_parComp_real_' parNames{i}],0);
    
end

%% results MM
results_MM = readtable('../../data/CML_modelFits/withoutHD.csv');
results_MM_AS = readtable('../../data/CML_modelFits/withHD.csv');

mean_M = [str2double(table2array(results_MM(1,2:3:end))),str2double(table2array(results_MM_AS(1,2)))];
std_M = [str2double(table2array(results_MM(1,3:3:end))),str2double(table2array(results_MM_AS(1,3)))];

CreatePlotCSV(0:length(mean_M)-1,mean_M*100,'../csvs/CML_MM',0,std_M*100)

acc_MM = [str2num(cell2mat(table2array(results_MM(1,4))))',str2num(cell2mat(table2array(results_MM(1,7))))',str2num(cell2mat(table2array(results_MM(1,10))))',str2num(cell2mat(table2array(results_MM(1,13))))'];
x = [-0.3*ones(size(acc_MM(:,1)));0.7*ones(size(acc_MM(:,1)));1.7*ones(size(acc_MM(:,1)));2.7*ones(size(acc_MM(:,1)))];
x = x+normrnd(0,0.02,size(x));
CreatePlotCSV(x,acc_MM(1:40)'*100,'../csvs/CML_MM_all',0)

acc_MM = [acc_MM,str2num(cell2mat(table2array(results_MM_AS(1,4))))'];

%% results LSTM

%% acurracies LSTM
for i=1:10
    tmp = table2array(readtable(['../../data/CML_NN/outputfullData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_fD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/CML_NN/outputnoiseData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/CML_NN/outputnoiseSparseData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nSD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/CML_NN/outputnoiseSparseUdData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nSND(i) = tmp(1);
    tmp = table2array(readtable(['../../data/CML_NN/outputnoiseSparseUdDataHD/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nAND(i) = tmp(1);
end
res = [res_fD',res_nD',res_nSD',res_nSND',res_nAND'];

mean_NN = mean(res);
std_NN = std(res);

CreatePlotCSV(0:4,mean_NN.*100,'../csvs/CML_NN',0,std_NN.*100)

x = [0.3*ones(size(res(:,1)));1.3*ones(size(res(:,1)));2.3*ones(size(res(:,1)));3.3*ones(size(res(:,1)))];
x = x+normrnd(0,0.02,size(x));

CreatePlotCSV(x,res(1:40)'*100,'../csvs/CML_NN_all',0)
% res_fD = table2array(readtable('../../data/CML_NN/outputfullData/res.csv'));
% res_fD = res_fD(:,3);
% CreatePlotCSV(zeros(size(res_fD)),res_fD*100,'../csvs/CML_NN_fD',0);
% res_nD = table2array(readtable('../../data/CML_NN/outputnoiseData/res.csv'));
% res_nD = res_nD(:,3);
% CreatePlotCSV(zeros(size(res_nD)),res_nD*100,'../csvs/CML_NN_nD',0);
% res_nSD = table2array(readtable('../../data/CML_NN/outputnoiseSparseData/res.csv'));
% res_nSD = res_nSD(:,3);
% CreatePlotCSV(zeros(size(res_nSD)),res_nSD*100,'../csvs/CML_NN_nSD',0);
% res_nSND = table2array(readtable('../../data/CML_NN/outputnoiseSparseUdData/res.csv'));
% res_nSND = res_nSND(:,3);
% CreatePlotCSV(zeros(size(res_nSND)),res_nSND*100,'../csvs/CML_NN_nSND',0);
% res_nAND = table2array(readtable('../../data/CML_NN/outputnoiseSparseUdDataHD/res.csv'));
% res_nAND = res_nAND(:,3);
% CreatePlotCSV(zeros(size(res_nAND)),res_nAND*100,'../csvs/CML_NN_nAND',0);
% 
% res = [res_fD,res_nD,res_nSD,res_nSND,res_nAND];
% 
% mean_NN = mean(res);
% std_NN = std(res);
% 
% CreatePlotCSV(0:4,mean_NN.*100,'../csvs/CML_NN',0,std_NN.*100)

%% Accuracies of all methods compraing AP and AS

acc_as = [acc_MM(:,[4,5]),acc_glm(:,[4,5]),res(:,[4,5])];
%acc_as = acc_as(:,[1,3,5,2,4,6]);

x = [-0.25 0.25 1 1.5 2.25 2.75];
x = repmat(x,10,1);
x = x+normrnd(0,0.02,size(x));

CreatePlotCSV(x(1:20),acc_as(1:20)*100,'../csvs/CML_AS_MM',0)
CreatePlotCSV(x(21:40),acc_as(21:40)*100,'../csvs/CML_AS_GLM',0)
CreatePlotCSV(x(41:60),acc_as(41:60)*100,'../csvs/CML_AS_NN',0)

CreatePlotCSV(reshape(x(:,[1,3,5]),1,[]),reshape(acc_as(:,[1,3,5])*100,1,[]),'../csvs/CML_AP',0);
CreatePlotCSV(reshape(x(:,[2,4,6]),1,[]),reshape(acc_as(:,[2,4,6])*100,1,[]),'../csvs/CML_AS',0);