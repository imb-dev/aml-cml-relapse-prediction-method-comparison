function [para_opt] = Modell_fit(data)
%fitting the model with only 2 free parameters
rng('shuffle')

filename = ['data/result_fit_' inputname(1) '.csv'];

if ~isfile(filename)
    fid = fopen(filename,'w');
    fprintf(fid,'%s,%s,%s,%s\n','PatientID','tl1','pl','fval');
    fclose(fid);
end

n=1;

r1_tl1 = 0.1;
r2_tl1 = 0.99;
r1_pl = 0.04;
r2_pl = 0.2;

ids = unique(data.id,'stable');

for j=1:n
    st_tl1 = (r2_tl1-r1_tl1)*rand(1)+r1_tl1;
    st_pl = (r2_pl-r1_pl)*rand(1)+r1_pl;
    
    for i=1:length(ids)
        disp(j)
        disp(i)
        load('data/model_parameters.mat','p')
        
        idx = find(data.id==ids(i));
        p.th_b = str2num(data.chemo_begin(idx(1)));
        p.th_e = str2num(data.chemo_end(idx(1)));
        p.th_n = length(p.th_e);
        data_exp(:,1) = double(table2array(data(idx,1)));
        data_exp(:,2) = table2array(data(idx,2));
        data_exp(data_exp(:,1)>9*30,:) = [];
        %if size(data_exp,1)<3; clear data_exp; continue; end
        %option = ['Algorithm','interior-point'];
        %[para,fval] = fmincon(@ToMin_Model_fun,[st_tl1 st_pl st_c],[],[],[],[],[r1_tl1 r1_pl r1_c],[r2_tl1 r2_pl r2_c],[],option,PatientID);
        problem = createOptimProblem('fmincon',...
            'objective',@(x)ToMin_Model_fun(x,p,data_exp),...
            'x0',[st_tl1 st_pl],...
            'lb',[r1_tl1 r1_pl],...
            'ub',[r2_tl1 r2_pl],...
            'options',optimoptions(@fmincon,'Algorithm','sqp','Display','off'));
        gs = MultiStart('UseParallel',true,'StartPointsToRun','bounds','Display','off');
        gs.TolFun=1e-2;
        gs.TolX=1e-3;
        tic
        [para,fval] = run(gs,problem,40);
        toc
        para_opt{j}(i,:) = para;
        %mini{j}(i) = fval;
        
        dlmwrite(filename,[i para fval],'delimiter',',','-append');
        clear para fval data_exp
    end
end

end
function [mini] = ToMin_Model_fun(tooptim,p,data_exp)

tl1 = tooptim(1);
pl = tooptim(2);

addpath('functions')

t_exp = data_exp(:,1);
x_exp = data_exp(:,2);
x_exp(t_exp>9*30) = [];
t_exp(t_exp>9*30) = [];

% bl_det = x_exp==-3;
% i_bl5 = find(bl_det==1);

tmax = 24*30;

p.pl = pl;
p.tl1 = tl1;

[t,x] = runmodel_th(p,tmax);
x_rel = (x(:,2)'+x(:,4)')./sum(x').*100;
x_rel(x_rel<1e-5) = 1e-5;

if t<tmax
    mini = 1e6;
elseif any(x_rel<0)
        mini = 1e6;
else
    for i=1:length(t_exp)
        x_sim(i) = x_rel(find(t>=t_exp(i),1));
    end
    
    mini = sum((log10(x_sim)-x_exp').^2);
    % doppelte Strafe f?r Abweichungen von Werten unterhalb Detektionslimit
%     if ~isempty(i_bl5)
%     mini = mini+sum((log10(x_sim(i_bl5))-x_exp(i_bl5)').^2);
%     end
end
end