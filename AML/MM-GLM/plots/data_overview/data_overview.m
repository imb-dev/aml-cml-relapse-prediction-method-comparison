% Generating csv files for the plotting of the different data sets used in
% the publication

clear all;
close all;

P = 16;

load('../../data/FullData.mat')
ids = unique(FullData.id,'stable');
idx = find(FullData.id==ids(P));

CreatePlotCSV(double(FullData.time(idx)')/30,10.^(FullData.log10NPM1_ABL(idx)'),'../csvs/data_D',0);

load('../../data/NoiseData.mat')
idx = find(NoiseData.id==ids(P));

CreatePlotCSV(double(NoiseData.time(idx)')/30,10.^(NoiseData.log10NPM1_ABL(idx)'),'../csvs/data_DN',0);

load('../../data/NoiseSparseData.mat')
idx = find(NoiseSparseData.id==ids(P));

CreatePlotCSV(double(NoiseSparseData.time(idx)')/30,10.^(NoiseSparseData.log10NPM1_ABL(idx)'),'../csvs/data_DNS',0);


load('../../data/NoiseSparseNDData.mat')
idx = find(NoiseSparseNDData.id==ids(P));

CreatePlotCSV(double(NoiseSparseNDData.time(idx)')/30,10.^(NoiseSparseNDData.log10NPM1_ABL(idx)'),'../csvs/data_AP',0);

load('../../data/NoiseAdvNDData.mat')
idx = find(NoiseAdvNDData.id==ids(P));

CreatePlotCSV(double(NoiseAdvNDData.time(idx)')/30,10.^(NoiseAdvNDData.log10NPM1_ABL(idx)'),'../csvs/data_AS',0);