function dxdt = modelfun(t,x,p)

ch = p.ch;
cl = p.cl;

% 1 - HA, 2 - LA, 3 - HO, 4 - LO

dxdt(1) = p.th2*x(3)*(1-(x(1)+x(2))/p.KA)-p.th1*x(1)*(1-(x(3)+x(4))/p.KO);
dxdt(2) = p.tl2*x(4)*(1-(x(1)+x(2))/p.KA)-p.tl1*x(2)*(1-(x(3)+x(4))/p.KO);
dxdt(3) = p.th1*x(1)*(1-(x(3)+x(4))/p.KO)-p.th2*x(3)*(1-(x(1)+x(2))/p.KA)+(1-(x(3)+x(4))/p.KO)*p.ph*x(3)-ch*x(3)-p.dh*x(3);
dxdt(4) = p.tl1*x(2)*(1-(x(3)+x(4))/p.KO)-p.tl2*x(4)*(1-(x(1)+x(2))/p.KA)+(1-(x(3)+x(4))/p.KO)*p.pl*x(4)-cl*x(4)-p.dl*x(4);

dxdt = dxdt';
end