clear all
close all

load('data/FullData.mat')

ids = unique(FullData.id,'stable');
rlps_par = double.empty(length(ids),2,0);
non_rlps_par = double.empty(length(ids),2,0);

for i=1:length(ids)
    idx = find(strcmp(ids(i),FullData.id));
    
    if strcmp(FullData.relapse(idx(1)),'True')
        rlps_par(i,:) = [FullData.tla(idx(1)) FullData.pl(idx(1))];
    else
        non_rlps_par(i,:) = [FullData.tla(idx(1)) FullData.pl(idx(1))];
    end
    
    chemo_num(i) =  double(FullData.chemo_num(idx(1)));
end

t1 = rlps_par(:,1);
t2 = non_rlps_par(:,1);

p1 = rlps_par(:,2);
p2 = non_rlps_par(:,2);

figure()
plot(t1,p1,'.r')
hold on
plot(t2,p2,'.b')

figure()
plot(t1./p1,'.r')
hold on
plot(t2./p2,'.b')