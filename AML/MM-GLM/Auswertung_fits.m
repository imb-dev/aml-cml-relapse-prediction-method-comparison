function [accuracy] = Auswertung_fits(Data)
% Auswertung der csv.files von dem Modell-fit an die verschiedenen Datensätze
addpath('functions')

data_fit = readtable(['data/result_fit_' inputname(1) '.csv']);

%ids_fit = data_fit.PatientID;
ids = unique(Data.id,'stable');
ids_fit = data_fit.PatientID;
%ids = ids(ids_fit);

for i=1:length(ids_fit)
    idx = find(Data.id==ids(ids_fit(i)));
    
    load('data/model_parameters.mat','p')
    p.th_b = str2num(Data.chemo_begin(idx(1)));
    p.th_e = str2num(Data.chemo_end(idx(1)));
    p.th_e(p.th_b>24*30) = [];
    p.th_b(p.th_b>24*30) = [];
    p.th_n = length(p.th_e);
    p.tl1 = data_fit.tl1(i);
    p.pl = data_fit.pl(i);
    
    rlps_flag_fit(i) = relapse_flag(p,24*30);
    rlps_flag_dat(i) = strcmp(Data.relapse(idx(1)),'True');
    
end
prediction = abs(rlps_flag_fit-rlps_flag_dat);

indices = crossvalind('Kfold',prediction,10);
for i=1:10
    test = (indices == i);
    accuracy(i) = 1-sum(prediction(test))/sum(test);
end

save(['data/acc_' inputname(1) '.mat'],'accuracy','rlps_flag_fit','rlps_flag_dat')