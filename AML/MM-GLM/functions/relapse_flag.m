function [rlps_flag] = relapse_flag(p,tmax)
% diced wether relapse happend within time tmax

if ~exist('tmax','var') || isempty(tmax); tmax = 24*30; end

[t,x] = runmodel_th(p,tmax);
x_rel = (x(:,2)+x(:,4))./sum(x,2).*100;

if x_rel(find(t>p.th_e(min([5 p.th_n])),1))<0.01 && x_rel(end)>=0.01
    rlps_flag = true;
else
    rlps_flag = false;
end