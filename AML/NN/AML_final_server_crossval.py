#!/usr/bin/env python
# coding: utf-8

import numpy as np
import math
import pandas as pd
import keras
from sklearn.model_selection import train_test_split
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Masking, LSTM, Bidirectional
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import pickle
import os
from joblib import Parallel, delayed
from sklearn.model_selection import StratifiedKFold


def load_data(data_name):
    data = np.load('data/'+data_name+'.npz')
    X = data['X']
    Y = data['Y']
    p = np.random.permutation(X.shape[0])
    X = X[p]
    Y = Y[p,:]

    return(X, Y)


def make_model():
    model = Sequential()
    model.add(Masking(mask_value=-1, input_shape=(None,2)))
    model.add(Bidirectional(LSTM(32,activation='relu')))
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dense(2))
    model.add(Activation('sigmoid'))

    opt = keras.optimizers.Adam(lr=1e-3,decay=1e-5)
    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    return(model)


def run_model(model, X_train, Y_train, data_name, run, k):
    checkpoint_path = 'output'+data_name+'/k'+str(k)+'/run'+str(run)
    os.makedirs(checkpoint_path, exist_ok=True)

    if not os.path.exists('output'+data_name):
        os.makedirs('output'+data_name)

    mcp_save = ModelCheckpoint(checkpoint_path + '/best_weights.hdf5', save_best_only=True, monitor='val_accuracy', mode='max')
    earlystop = EarlyStopping(monitor='val_loss', mode='min', verbose=0, patience=20)

    h = model.fit(X_train, Y_train, validation_split=0.15, batch_size=128, epochs=100, verbose=0, callbacks=[mcp_save,earlystop])

    with open(checkpoint_path + '/history.pkl', 'wb') as f:
        pickle.dump(h.history, f)

    #print(max(h.history['val_acc']))
    model.load_weights(checkpoint_path + '/best_weights.hdf5')

    return model, h

#datasets = ['fullData', 'noiseData', 'noiseSparseData', 'noiseSparseNDData', 'noiseAdvNDData' ]
datasets = ['noiseSparseData', 'noiseSparseNDData', 'noiseAdvNDData' ]
repetitions = 10

for data_name in datasets:
    print(f"Working on dataset {data_name}")
    X, Y = load_data(data_name)

    def single_run(X_train, Y_train, X_test, Y_test, data_name, run, k):
        model = make_model()
        print("Now training, k="+str(k)+', run = '+str(run))
        model, hist = run_model(model, X_train, Y_train, data_name, run, k)
        print("Now evaluating")
        score = model.evaluate(X_test, Y_test, verbose=0)
        va = max(hist.history['val_accuracy'])
        vl = hist.history['val_loss'][hist.history['val_accuracy'].index(max(hist.history['val_accuracy']))]
        return score, va, vl
    
    kfold = StratifiedKFold(n_splits=10, shuffle=True)
    k = 1
    for train, test in kfold.split(X, Y[:,0]):
        scores_va = Parallel(n_jobs=repetitions)(delayed(single_run)(X[train], Y[train], X[test], Y[test], data_name,run, k) for run
            in range(repetitions))
        
        idxs = []
        tas = []
        vas = []
        vls = []

        for idx, (ta, va , vl) in enumerate(scores_va):
            idxs.append(idx)
            tas.append(ta[1])
            vas.append(va)
            vls.append(vl)

        d = {'idx': idxs,
                'ta': tas,
                'va': vas,
                'vl': vls}
        df = pd.DataFrame(data=d)
        df.to_csv('output'+data_name+'/k'+str(k)+'/res.csv')
    
        k = k+1

    

