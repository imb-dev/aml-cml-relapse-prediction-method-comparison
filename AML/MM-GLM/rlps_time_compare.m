% comparing relapse time estimated by the model, estimated using the data
% and the true relapse time
clear all
close all
addpath('../functions')

fit_dat = readtable(['data/result_fit_NoiseSparseNDData.csv']);
load(['data/NoiseSparseNDData.mat'])
data=NoiseSparseNDData;

ids = unique(data.id,'stable');

Pats = table2array(unique(fit_dat(:,1)));
n = length(Pats);

param_fit = table2array(fit_dat(:,2:3));

load('data/model_parameters.mat')
%% relapse time accordance
% how well true relapse, relapse from the data and relapse from the fit
% accord

for i=1:n
    ind = find(data.id==ids(i));
    
    p.th_n = data.chemo_num(ind(1));
    p.th_b = str2num(data.chemo_begin(ind(1)));
    p.th_e = str2num(data.chemo_end(ind(1)));
    
    time_r_true(i) = data.rlps_time(ind(1));
    time_r_fit(i) = relapse_time(param_fit(i,:),720,p);
    
    %corrected relapse time from data, oriented on relapse time estimation
    %from real data
    if any(data.log10NPM1_ABL(ind)<-2) %if remission is reached
        i_ref = find(data.log10NPM1_ABL(ind)<-2,1);
        
        th_diff = p.th_b(2:end)-p.th_e(1:end-1);
        end_prim_th = find(th_diff>120,1);           %end of primary therapy is when therapy distance is at least 6 months
        if isempty(end_prim_th) end_prim_th = p.th_n; end
        i_th = find(data.time(ind)>=p.th_e(end_prim_th),1);
        if isempty(i_th); i_th = length(ind); end
        
        i_r = find(data.log10NPM1_ABL(ind)>-2);
        i_r = i_r(find(i_r>i_ref & i_r>i_th,1));
        if ~isempty(i_r) && i_r>1
            time_r_dat(i) = interp1([data.log10NPM1_ABL(ind(i_r-1)) data.log10NPM1_ABL(ind(i_r))],[data.time(ind(i_r-1)) data.time(ind(i_r))],-2,'linear','extrap');%(-2-ne)/m;  %x = (y-n)/m
            if time_r_dat(i)<0
                time_r_dat(i) = NaN;
            end
        elseif data{Pats(i)}(2,end)>=720; time_r_dat(i) = 720;
        else time_r_dat(i) = NaN;
        end
    else
        time_r_dat(i) = 0;
    end
        
end

time_r_true(isinf(time_r_true) | time_r_true>720) = 720;
time_r_fit(isinf(time_r_fit) | time_r_fit>720) = 720;
time_r_dat(time_r_dat>720) = 720;

time_r_true_n = time_r_true(isnan(time_r_dat));
time_r_fit_n = time_r_fit(isnan(time_r_dat));
time_r_dat_n = cellfun(@(x) x(2,end),data);
time_r_dat_n = time_r_dat_n(isnan(time_r_dat));

time_r_true(isnan(time_r_dat)) = [];
time_r_fit(isnan(time_r_dat)) = [];
time_r_dat(isnan(time_r_dat)) = [];

div_true_dat = abs(time_r_true-time_r_dat);
div_true_fit = abs(time_r_true-time_r_fit);
div_fit_dat = abs(time_r_fit-time_r_dat);

strong_div_td = length(find(div_true_dat>=30*6))/(length(time_r_true)-sum(isnan(time_r_dat)));
strong_div_tf = length(find(div_true_fit>=30*6))/length(time_r_true);
strong_div_fd = length(find(div_fit_dat>=30*6))/(length(time_r_true)-sum(isnan(time_r_dat)));

abs_sq_err(1) = sum((time_r_true'-time_r_dat').^2,'omitnan');
abs_sq_err(2) = sum((time_r_true'-time_r_fit').^2,'omitnan');

me_sq_err(1) = mean((time_r_true'-time_r_dat').^2,'omitnan');
me_sq_err(2) = mean((time_r_true'-time_r_fit').^2,'omitnan');

acc_true_dat = length(find(abs(div_true_dat)<=0.2))/length(div_true_dat);

ccc1 = f_CCC([time_r_true; time_r_dat]',0.05);
ccc2 = f_CCC([time_r_true; time_r_fit]',0.05);

h = figure('Color','w','Units','centimeters','Position',[15 15 15 7.5]);
subplot(1,2,1)
scatter(time_r_true,time_r_fit)
xlabel('true')
ylabel('from fit')
hold on
plot([min(time_r_true) max(time_r_true)],[min(time_r_true) max(time_r_true)],'--')

subplot(1,2,2)
scatter(time_r_true,time_r_dat)
xlabel('true')
ylabel('from data')
hold on
plot([min(time_r_true) max(time_r_true)],[min(time_r_true) max(time_r_true)],'--')

CreatePlotCSV(time_r_true/30,time_r_fit/30,'../csvs/compareFitTrue',0);
CreatePlotCSV(time_r_true/30,time_r_dat/30,'../csvs/compareDatTrue',0);
CreatePlotCSV(time_r_true_n/30,time_r_fit_n/30,'../csvs/compareFitTrue_n',0);
CreatePlotCSV(time_r_true_n/30,time_r_dat_n/30,'../csvs/compareDatTrue_n',0);