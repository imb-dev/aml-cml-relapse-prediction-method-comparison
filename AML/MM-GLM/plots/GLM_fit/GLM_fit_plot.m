function [] = GLM_fit_plot()

daten = 'NoiseAdvND';%'FullData'

load(['../../data/' daten 'Data.mat'])
Data = eval([daten 'Data']);

P=19; %50
ids = unique(Data.id,'stable');
idx = find(Data.id==ids(P));

x = double(Data.time(idx));
y = Data.log10NPM1_ABL(idx);
chemo = str2num(Data.chemo_begin(idx(1)));
chemo = [chemo; str2num(Data.chemo_end(idx(1)))];
chemo(:,chemo(1,:)>9*30) = [];

load('../../GLMparFit.mat',['par_' daten])
par = eval(['par_' daten]);

[x_f,y_f] = Fit(par(P,:),chemo,1);

figure()
plot(x,y,'bx')
hold on
plot(x_f,y_f)

CreatePlotCSV(x'/30,10.^y','../csvs/GLMfit_exp',0);
CreatePlotCSV(x_f/30,10.^y_f,'../csvs/GLMfit_sim',0);


%% elimination slope alpha line for plot

x_alpha = -5:60;
y_alpha = -0.0722*x_alpha+1.173;

CreatePlotCSV(x_alpha'/30,10.^y_alpha,'../csvs/GLM_alpha',0);

end
function [x_f,y_f] = Fit(par,chemo,ud)
a = par(1);
c = par(2);
y0 = par(3);

ende = 9*30;%chemo(2,end)+21;
chemo_num = size(chemo,2);
x_f = 1:ende;
y_f = y0;

for i=1:chemo_num
    %chemo on
    y_f = [y_f -a*x_f(chemo(1,i)+1:chemo(2,i))+y0]; %y = -a*x+y0; x von chemo start bis chemo Ende 
    %chemo off
    y0 = y_f(end)-c*x_f(chemo(2,i)); %y0 ist Endpunkt des letzten Teilstücks
    if i<chemo_num
        y_f = [y_f c*x_f(chemo(2,i)+1:chemo(1,i+1))+y0];
        y0 = y_f(end)+a*x_f(chemo(1,i+1));
    else
        y_f = [y_f c*x_f(chemo(2,i)+1:end)+y0];
    end
    
end

x_f = [x_f-1 9*30];

if ud
    y_f(y_f<-5) = -5;
end

end