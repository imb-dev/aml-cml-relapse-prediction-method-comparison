% plotting the accuracies of the mechanistic model for the respective data
% sets

clear all;
close all;

load('../../data/acc_FullData.mat')
acc = mean(accuracy);
std_acc = std(accuracy);

acc_mm = accuracy;

load('../../data/acc_NoiseData.mat')
acc = [acc mean(accuracy)];
std_acc = [std_acc std(accuracy)];

acc_mm = [acc_mm' accuracy'];

load('../../data/acc_NoiseSparseData.mat')
acc = [acc mean(accuracy)];
std_acc = [std_acc std(accuracy)];

acc_mm = [acc_mm accuracy'];

load('../../data/acc_NoiseSparseNDData.mat')
acc = [acc mean(accuracy)];
std_acc = [std_acc std(accuracy)];

acc_mm = [acc_mm accuracy'];

load('../../data/acc_NoiseAdvNDData.mat')
acc = [acc mean(accuracy)];
std_acc = [std_acc std(accuracy)];

acc_mm = [acc_mm accuracy'];
mean_acc_mm = acc; 
CreatePlotCSV(0:4,acc*100,'../csvs/AML_MM',0,std_acc*100);

x = [-0.3*ones(size(acc_mm(:,1)));0.7*ones(size(acc_mm(:,1)));1.7*ones(size(acc_mm(:,1)));2.7*ones(size(acc_mm(:,1)))];
x = x+normrnd(0,0.02,size(x));
CreatePlotCSV(x,acc_mm(1:40)'*100,'../csvs/AML_MM_all',0)


%csvwrite('../csvs/AML_MM_all.csv',acc_mm);
%% acurracies GLM

load('../../GLM_acc.mat')

%csvwrite('../csvs/AML_GLM_all.csv',acc);

accur = mean(acc(:,[1,2,3,4,5]));%./acc_mm;
std_g = std(acc(:,[1,2,3,4,5]));%./acc_mm;

CreatePlotCSV(0:4,accur*100,'../csvs/AML_GLM',0,std_g*100)
x = [zeros(size(acc(:,1)));ones(size(acc(:,1)));2*ones(size(acc(:,1)));3*ones(size(acc(:,1)))];
x = x+normrnd(0,0.02,size(x));
CreatePlotCSV(x,acc(1:40)'*100,'../csvs/AML_GLM_all',0)

%% acurracies LSTM

for i=1:10
    tmp = table2array(readtable(['../../data/AML_NN/outputfullData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_fD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/AML_NN/outputnoiseData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/AML_NN/outputnoiseSparseData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nSD(i) = tmp(1);
    tmp = table2array(readtable(['../../data/AML_NN/outputnoiseSparseNDData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nSND(i) = tmp(1);
    tmp = table2array(readtable(['../../data/AML_NN/outputnoiseAdvNDData/k' num2str(i) '/res.csv']));
    tmp = tmp(tmp(:,4)==max(tmp(:,4)),[3]);
    res_nAND(i) = tmp(1);
end
res = [res_fD',res_nD',res_nSD',res_nSND',res_nAND'];

mean_NN = mean(res);
std_NN = std(res);

x = (0:4)+0.3;
CreatePlotCSV(x,mean_NN.*100,'../csvs/AML_NN',0,std_NN.*100)

x = [0.3*ones(size(res(:,1)));1.3*ones(size(res(:,1)));2.3*ones(size(res(:,1)));3.3*ones(size(res(:,1)))];
x = x+normrnd(0,0.02,size(x));

CreatePlotCSV(x,res(1:40)'*100,'../csvs/AML_NN_all',0)

%csvwrite('../csvs/AML_NN_all.csv',res)

%% Accuracies of all methods compraing AP and AS

acc_as = [acc_mm(:,[4,5]),acc(:,[4,5]),res(:,[4,5])];
%acc_as = acc_as(:,[1,3,5,2,4,6]);

x = [-0.25 0.25 1 1.5 2.25 2.75];
x = repmat(x,10,1);
x = x+normrnd(0,0.02,size(x));

CreatePlotCSV(x(1:20),acc_as(1:20)*100,'../csvs/AML_AS_MM',0)
CreatePlotCSV(x(21:40),acc_as(21:40)*100,'../csvs/AML_AS_GLM',0)
CreatePlotCSV(x(41:60),acc_as(41:60)*100,'../csvs/AML_AS_NN',0)

CreatePlotCSV(reshape(x(:,[1,3,5]),1,[]),reshape(acc_as(:,[1,3,5])*100,1,[]),'../csvs/AML_AP',0);
CreatePlotCSV(reshape(x(:,[2,4,6]),1,[]),reshape(acc_as(:,[2,4,6])*100,1,[]),'../csvs/AML_AS',0);

% acc_as = [acc_mm(:,5),acc(:,5),res(:,5)];
% acc_ap = [acc_mm(:,4),acc(:,4),res(:,4)];
% 
% x_as = [-0.15 0.85 1.85];
% x_as = repmat(x_as,10,1);
% x_as = x_as+normrnd(0,0.02,size(x_as));
% 
% CreatePlotCSV(x_as(1:end),acc_as(1:end)*100,'../csvs/AML_AS',0)