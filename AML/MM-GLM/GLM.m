% GLM for predicting relapse within two years in AML
function [acc]=GLM()

par_fit = false;

%% fitting to linear functions to course during therapy

load('data/FullData.mat')
load('data/NoiseData.mat')
load('data/NoiseSparseData.mat')
load('data/NoiseSparseNDData.mat')
load('data/NoiseAdvNDData.mat')
load('data/RealData.mat')

if par_fit == true
    load('GLMparFit.mat')
    par_Full = Fitting(FullData,0);
    save('GLMparFit.mat','par_Full')
    %
    par_Noise = Fitting(NoiseData,0);
    save('GLMparFit.mat','par_Full','par_Noise')
    %
    par_NoiseSparse = Fitting(NoiseSparseData,0);
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse')
    
    par_NoiseSparseND = Fitting(NoiseSparseNDData,1);
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse','par_NoiseSparseND')
    
    par_NoiseAdvND = Fitting(NoiseAdvNDData,1);
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse','par_NoiseSparseND','par_NoiseAdvND')
    
    par_Real = Fitting(RealData,1);
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse','par_NoiseSparseND','par_NoiseAdvND','par_Real')
    
    par_Full(:,4:5) = alpha_n(FullData);
    par_Noise(:,4:5) = [alpha_n(NoiseData)];
    par_NoiseSparse(:,4:5) = [alpha_n(NoiseSparseData)];
    par_NoiseSparseND(:,4:5) = [alpha_n(NoiseSparseNDData)];
    par_NoiseAdvND(:,4:5) = [alpha_n(NoiseAdvNDData)];
    par_Real(:,4:5) = [alpha_n(RealData)];
    
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse','par_NoiseSparseND','par_NoiseAdvND','par_Real')
elseif par_fit=='alpha' 
    load('GLMparFit.mat')
    
    par_Full(:,4:5) = alpha_n(FullData);
    par_Noise(:,4:5) = [alpha_n(NoiseData)];
    par_NoiseSparse(:,4:5) = [alpha_n(NoiseSparseData)];
    par_NoiseSparseND(:,4:5) = [alpha_n(NoiseSparseNDData)];
    par_NoiseAdvND(:,4:5) = [alpha_n(NoiseAdvNDData)];
    par_Real(:,4:5) = [alpha_n(RealData)];
    
    save('GLMparFit.mat','par_Full','par_Noise','par_NoiseSparse','par_NoiseSparseND','par_NoiseAdvND','par_Real')
else
    load('GLMparFit.mat')
end
%% fitting the GLM to the data sets


acc_Full = GLM_fun(FullData,par_Full);
acc_Noise = GLM_fun(NoiseData,par_Noise);
acc_NoiseSparse = GLM_fun(NoiseSparseData,par_NoiseSparse);
acc_NoiseSparseND = GLM_fun(NoiseSparseNDData,par_NoiseSparseND);
acc_NoiseAdvND = GLM_fun(NoiseAdvNDData,par_NoiseAdvND);
acc_Real = GLM_fun(RealData,par_Real);

acc = [acc_Full' acc_Noise' acc_NoiseSparse' acc_NoiseSparseND' acc_NoiseAdvND' acc_Real'];

save('GLM_acc','acc')
end
%% alpha and n calculation
function [par] = alpha_n(data)

ids = unique(data.id,'stable');

for i=1:length(ids)
    idx = find(data.id==ids(i));
    
    idx_rem = find(data.log10NPM1_ABL(idx)<-2,1);
    if idx_rem==1 & length(idx)>1; idx_rem=2; end
    %lin regression between frist point and first rem point
    if ~isempty(idx_rem)
        alpha(i) = (data.log10NPM1_ABL(idx(idx_rem))-data.log10NPM1_ABL(idx(1)))/double((data.time(idx(idx_rem))-data.time(idx(1))));
    else
        alpha(i)=NaN;
    end
    n(i)=min(data.log10NPM1_ABL(idx));
end

par = [alpha' n'];
end
%% GLM fit
function [acc] = GLM_fun(data,par)
% x1 - slope1; x2 - slope2; x3 - starting point; x4 - elimination slope; x5
% - deepest value
[~,idx_id,~] = unique(data.id,'stable');
y = ones(size(idx_id));
y(data.relapse(idx_id)=="False") = 0;

indices = crossvalind('Kfold',y,10);
for i=1:10
    test = (indices == i);
    train = ~test;
    mdl = fitglm(par(train,:),y(train),'logit(y) ~ x1+x2+x5+x2*x5','Distribution','binomial'); %,'logit(y) ~ x1+x2+x3+x4+x5'
    
    cp = classperf(y);
    y_pred = round(predict(mdl,par(test,:)));
    classperf(cp,y_pred,test);
    
    acc(i) = 1-cp.ErrorRate;
end

end
function [par]=Fitting(data,ud)

ids = unique(data.id,'Stable');
disp('start fitting ...')
% fitting to generate parameters for GLM
parfor i=1:length(ids)
    ind = find(data.id==ids(i));
    %disp(i)
    x = data.time(ind);
    y = data.log10NPM1_ABL(ind);
    
    chemo = str2num(data.chemo_begin(ind(1)));
    chemo = [chemo; str2num(data.chemo_end(ind(1)))];
    chemo(:,chemo(1,:)>9*30) = [];
    
    problem = createOptimProblem('fmincon',...
            'objective',@(p)Fit_fun(p,x,y,chemo,ud),...
            'x0',[-0.5 0.5 2.5],...
            'lb',[-0.1 -0.1 -4],...
            'ub',[1 0.5 3],...
            'options',optimoptions(@fmincon,'Algorithm','sqp','Display','off'));
        gs = GlobalSearch('Display','off');
        par(i,:) = run(gs,problem);
    %par(i,:) = fminunc(@Fit_fun,[1,1,1.5],[],x,y,chemo,ud);
    %waitbar(i/length(ids))
end

disp('Fitting finished')
end
function [min] = Fit_fun(par,x,y,chemo,ud)
a = par(1);
c = par(2);
y0 = par(3);

ende = 9*30;%chemo(2,end)+21;
chemo_num = size(chemo,2);
x_f = 1:ende;
y_f = y0;

y(x>ende) = [];
x(x>ende) = [];

% if ud
%     x(y==-5) = [];
%     y(y==-5) = [];
% end

for i=1:chemo_num
    %chemo on
    y_f = [y_f -a*x_f(chemo(1,i)+1:chemo(2,i))+y0]; %y = -a*x+y0; x von chemo start bis chemo Ende 
    %chemo off
    y0 = y_f(end)-c*x_f(chemo(2,i)); %y0 ist Endpunkt des letzten Teilstücks
    if i<chemo_num
        y_f = [y_f c*x_f(chemo(2,i)+1:chemo(1,i+1))+y0];
        y0 = y_f(end)+a*x_f(chemo(1,i+1));
    else
        y_f = [y_f c*x_f(chemo(2,i)+1:end)+y0];
    end
    
end

%x_f = [0 x_f];

if ud
    y_f(y_f<-5) = -5;
end

min = sum((y_f(x+1)-y').^2);

end