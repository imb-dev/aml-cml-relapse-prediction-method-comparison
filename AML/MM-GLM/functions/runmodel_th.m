function [time,xe] = runmodel_th(p,tmax)

scale = 1e8;

p.KA = scale*p.KA;
p.KO = scale*p.KO;

ch = p.ch;
cl = p.cl;
pl = p.pl;
tl1 = p.tl1;
tl2 = p.tl2;

% create steady state
p.ch = 0;
p.cl = 0;
p.pl = p.ph;
p.tl1 = p.th1;
%p.tl2 = p.th2;

k = 0.60;
l = 0.60;
x0 = [k*p.KA (1-k)*p.KA l*p.KO (1-l)*p.KO];
tspan = [0 2000];
[~,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0);
x0 = x(end,:);

p.pl = pl;
p.tl1 = tl1;
p.tl2 = tl2;

options = odeset('RelTol',1e-3,'NonNegative',1:4);

%ignor therapies after tmax
p.th_n = p.th_n-length(find(p.th_b>tmax));

time = [];  %time vector
xe = [];    %solution vector/matrix

if p.th_n>0
    %t1 = 0;
    tspan = [0 p.th_e(1)];
    
    
    for i=1:p.th_n
        
        %t2 = p.th_e(i);
        %tspan = [t1 t2];
        
        p.ch=ch;
        p.cl=cl;
        [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
        time = [time; t];
        xe = [xe; x];
        
        t1 = tspan(2);
        if i==p.th_n; t2 = tmax;
        else if p.th_b(i+1)>tmax; t2 = tmax;
            else t2 = p.th_b(i+1);
            end
        end
        if i==p.th_n || p.th_b(i+1)-p.th_e(i)>0 %wenn nicht direkt an einen Therapiezyklus der n?chste anschlie?t
            tspan = [t1 t2];
            x0 = x(end,:);
            p.ch = 0;
            p.cl = 0;
            [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
            time = [time; t];
            xe = [xe; x];
        end
        
        if tspan(2)<tmax
            t1 = t2;
            t2 = p.th_e(i+1);
            tspan = [t1 t2];
            x0 = x(end,:);
        end
        
    end
    
else
    tspan = [0 tmax];
    p.ch=ch;
    p.cl=cl;
    [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
    time = [time; t];
    xe = [xe; x];
end

xe = xe./scale;

end