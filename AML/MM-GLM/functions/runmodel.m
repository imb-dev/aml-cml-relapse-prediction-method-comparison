function [time,xe] = runmodel(p,tmax)

if ~isfield(p,'th_l'); p.th_l = 7; end
if ~isfield(p,'th_i'); p.th_i = 14; end

p.th_b = 0;

for i=1:p.th_n-1
    p.th_e(i) = p.th_b(i) + p.th_l;
    p.th_b(i+1) = p.th_e(i)+p.th_i;
end
p.th_e(p.th_n) = p.th_b(p.th_n) + p.th_l;

[time,xe] = runmodel_th(p,tmax);

% ch = p.ch;
% cl = p.cl;
% pl = p.pl;
% tl1 = p.tl1;
% tl2 = p.tl2;
% 
% % create steady state
% p.ch = 0;
% p.cl = 0;
% p.pl = p.ph;
% p.tl1 = p.th1;
% p.tl2 = p.th2;
% 
% options = odeset('NonNegative',1:4);
% 
% k = 0.60;
% l = 0.60;
% x0 = [k*p.KA (1-k)*p.KA l*p.KO (1-l)*p.KO];
% tspan = [0 2000];
% [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
% x0 = x(end,:);
% 
% p.pl = pl;
% p.tl1 = tl1;
% p.tl2 = tl2;
% 
% time = [];  %time vector
% xe = [];    %solution vector/matrix
% 
% if p.th_n>0 && tmax>p.th_l; tspan = [0 p.th_l]; else tspan = [0 tmax]; end
% 
% for i=1:p.th_n
%     
%     p.ch=ch;
%     p.cl=cl;
%     [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
%     time = [time; t];
%     xe = [xe; x];
%     
%     if t(end)==tmax; break; end
%     
%     t1 = tspan(2);
%     if i==p.th_n || t1+p.th_i>tmax; t2 = tmax; else t2 = t1+p.th_i; end
%     tspan = [t1 t2];
%     x0 = x(end,:);
%     p.ch = 0;
%     p.cl = 0;
%     [t,x] = ode45(@(t,x) modelfun21(t,x,p),tspan,x0,options);
%     time = [time; t];
%     xe = [xe; x];
%     
%     if t(end)==tmax; break; end
%     
%     t1 = t2;
%     t2 = t1+p.th_l;
%     tspan = [t1 t2];
%     x0 = x(end,:);
%     
% end

end