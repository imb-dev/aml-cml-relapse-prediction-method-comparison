function [] = csv_extract(dataname)

data = readtable(['data/result_fit_' dataname '.csv']);

param = table2array(data(:,[2 3]));
fval = table2array(data(:,4));

save(['data/result_fit_' dataname '.mat'],'param','fval')

h = figure('Color','w','Units','centimeters','Position',[15 15 15 7.5]);
subplot(1,3,1)
hist(param(:,1))
xlim([0.1 0.99])
title('tlo')
subplot(1,3,2)
hist(param(:,2))
xlim([0.04 0.2])
title('pl')
subplot(1,3,3)
hist(param(:,1)./param(:,2))
xlim([0.1/0.2 0.99/0.04])
title('tlo/pl')

pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperSize',[pos(3), pos(4)])

