function [time_r] = relapse_time(par,tmax,p)

if ~exist('tmax','var') || isempty(tmax); tmax = 5000; end

tlo = par(1);
pl = par(2);

if isnan(tlo) || isnan(pl); time_r = NaN; return; end

p.pl = pl;
p.tl1 = tlo;

[t,x] = runmodel(p,tmax);
i100 = find(t>100,1);

x_rel = log10((x(:,2)'+x(:,4)')./sum(x').*100);
[t,ia,~] = unique(t);
x_rel = x_rel(ia);

if any(x_rel<=-2)
    i_rem = find(x_rel<=-2,1);
    i100 = max([i100 i_rem]);
    i_rlps = find(x_rel(i100:end)>=-2,1)+i100-1;
    if isempty(i_rlps)
        time_r=Inf; 
    else
    time_r = interp1(x_rel(i_rlps-1:i_rlps+1),t(i_rlps-1:i_rlps+1),-2,'linear','extrap');%t(i_rlps);
    end
else time_r = 0;
end

end