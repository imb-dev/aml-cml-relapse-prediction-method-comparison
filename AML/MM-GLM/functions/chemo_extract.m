function [chemo_str] = chemo_extract(data)

chemo_str = cell2mat(cellfun(@(x) x,{data.Chemo},'UniformOutput',false));

for i=1:size(data,2)
    
    %remove therapies that begin later than 200 days.
    i_rm = find(chemo_str(i).begin>200);
    
    if ~isempty(i_rm)
    chemo_str(i).begin = chemo_str(i).begin(1:i_rm-1);
    chemo_str(i).end = chemo_str(i).end(1:i_rm-1);
    chemo_str(i).num = length(chemo_str(i).end);
    end
    
end

end