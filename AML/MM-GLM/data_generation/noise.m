function [] = noise(total)

%total = false;

if total
    load('../data/FullData_100k.mat','FullData')
else
    load('../data/FullData.mat','FullData')
end
NoiseData = FullData;
%% add noise as a standard error of 0.5 log10% as found in the data

rng(1)
error = normrnd(0,0.5,size(NoiseData.log10NPM1_ABL));

NoiseData.log10NPM1_ABL = NoiseData.log10NPM1_ABL+error;

if total
    save('../data/NoiseData_100k.mat','NoiseData')
    %writetable(NoiseData,'../../pythonfiles/data/NoiseData_100k.csv')
else
    save('../data/NoiseData.mat','NoiseData')
    %writetable(NoiseData,'../../pythonfiles/data/NoiseData.csv')
end