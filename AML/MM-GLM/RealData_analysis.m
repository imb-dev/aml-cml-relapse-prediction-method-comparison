clear all
close all

load('data/RealData.mat')

[ids,ia,~] = unique(RealData.id,'stable');

rlps_num = length(intersect(ia,find(strcmp('True',RealData.relapse))));