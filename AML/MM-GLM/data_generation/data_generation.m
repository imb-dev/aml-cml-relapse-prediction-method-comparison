clear all
close all

disp('start...')
% best(5000);
disp('best done')
% noise(false);
disp('noise done...')
noise_sparse(false);
disp('noise sparse done..')
noise_sparse_nd(false);
disp('noise sparse nd done')
noise_advanced_scheme_nd(false);
disp('all 5000 done')

disp('start 100.000')
best(100000);
disp('best 100k done')
noise(true);
disp('noise 100k done')
noise_sparse(true);
disp('sparse 100k done')
noise_sparse_nd(true);
disp('nd 100k done')
noise_advanced_scheme_nd(true);
disp('all done...')