function []=best(n)

load('../data/data.mat','data')
load('../data/result_fit_final.mat','param')
addpath('../functions')

n=n/2; %number of relapse and non-relapse patients. total: 2*n

chemo_str = chemo_extract(data);
load('../data/model_parameters.mat','p')

%for reproducability
s = RandStream('mt19937ar','Seed',1);
RandStream.setGlobalStream(s);
rng(1);

n_rlps = 0;
n_nonrlps = 0;
VarNames = {'time','log10NPM1_ABL','tla','pl','chemo_num','chemo_begin','chemo_end' 'rlps_time' 'relapse' 'maxTime' 'id'};
Data_rlps = table('Size',[0 11],'VariableTypes',{'int16' 'double' 'double' 'double' 'int8' 'string' 'string' 'double' 'string' 'int16' 'string'},...
    'VariableNames',VarNames);
Data_nonrlps = table('Size',[0 11],'VariableTypes',{'int16' 'double' 'double' 'double' 'int8' 'string' 'string' 'double' 'string' 'int16' 'string'},...
    'VariableNames',VarNames);

while n_rlps<n || n_nonrlps<n
    
    %pick parameter values from one patient and add some noise
    par = datasample(s,param,1);
    p.tl1 = par(1)+normrnd(0,0.01);
    p.pl = par(2)+normrnd(0,0.01);
    % guarantee boundaries
    if p.tl1>0.99; p.tl1 = 0.99; end
    if p.tl1<0.01; p.tl1 = 0.01; end
    if p.pl>0.2; p.pl = 0.2; end
    if p.pl<0.04; p.pl = 0.04; end
    
    %pick chemotherapy from another patient
    chemo = datasample(s,chemo_str,1);
    p.th_b = chemo.begin;
    p.th_e = chemo.end;
    p.th_n = chemo.num;
    
    %run model
    [t,x] = runmodel_th_teq(p,720);
    %reduce lengths
    t = t(1:7:end);
    x = x(1:7:end,:);
    x_rel = (x(:,2)+x(:,4))./sum(x,2).*100;
    
    i_9m = find(t>=9*30,1)-1;
    
    %relapse yes/no? yes if: last point after chemo is rem and thereafter
    %comes a point above 1
    if x_rel(find(t>chemo.end(min([5 chemo.num])),1))<0.01 && x_rel(end)>=0.01
        rlps_flag = 1;
        if n_rlps == n; continue; end
        
        %estimate time of relapse
        t_rem = t(find(x_rel<0.01,1));
        t_rlps = t(find(x_rel>0.01 & t>t_rem & t>chemo.end(min([5 chemo.num])),1));
        
        if t_rlps<=9*30; continue; end
        n_rlps = n_rlps+1;
        
        Data_rlps = [Data_rlps;table(t(1:i_9m),log10(x_rel(1:i_9m)),repmat(p.tl1,i_9m,1),repmat(p.pl,i_9m,1),repmat(chemo.num,i_9m,1),repmat(num2str(chemo.begin),i_9m,1),repmat(num2str(chemo.end),i_9m,1),...
            repmat(t_rlps,i_9m,1),repmat('True',i_9m,1),repmat(max(t(1:i_9m)),i_9m,1),repmat([num2str(n_rlps) 'r'],i_9m,1),'VariableNames',VarNames)];
    elseif x_rel(find(t>chemo.end(min([5 chemo.num])),1))>=0.01
        continue;
    else
        rlps_flag = 0;
        if n_nonrlps == n; continue; end
        n_nonrlps = n_nonrlps+1;
        
        %no remission or no relapse?
        if x_rel(find(t>chemo.end(min([5 chemo.num])),1))<0.01
            t_rlps = Inf;
        else
            t_rlps = 0;
        end
        
        Data_nonrlps = [Data_nonrlps;table(t(1:i_9m),log10(x_rel(1:i_9m)),repmat(p.tl1,i_9m,1),repmat(p.pl,i_9m,1),repmat(chemo.num,i_9m,1),repmat(num2str(chemo.begin),i_9m,1),repmat(num2str(chemo.end),i_9m,1),...
            repmat(t_rlps,i_9m,1),repmat('False',i_9m,1),repmat(max(t(1:i_9m)),i_9m,1),repmat([num2str(n_nonrlps) 'nr'],i_9m,1),'VariableNames',VarNames)];
    end
    
end

FullData = [Data_rlps;Data_nonrlps];

if n<50000
    save('../data/FullData.mat','FullData')
else
    save('../data/FullData_100k.mat','FullData')
end
%writetable(FullData,'../../pythonfiles/data/FullData.csv')